import requests
import re
from bs4 import BeautifulSoup
import helpers
import mtsbu_extractor


def get_page_source(car_number):
    url = f'https://baza-gai.com.ua/nomer/{car_number.upper()}'
    response = requests.get(url)
    return response


def extract_info_package(car_number):
    text = get_page_source(car_number).text
    bs = BeautifulSoup(text, features='html.parser')
    info = {}
    try:
        vin = re.search(
            r"<script>initializeCopyToClipboard\('([A-Z0-9]+)', 'btn-copy-big-card'\)<\/script>", text
        )

        # vin = None

        if vin:
            info['vin'] = vin.group(1)
        else:
            mtsbu = mtsbu_extractor.MTSBUExtractor()
            info['vin'] = mtsbu.get_vin(car_number)

        model = bs.find(
            'h4', {'class': 'plate-model-card__content-title'}).text

        info['model'] = helpers.clear_spaces(model)

        year = bs.find(
            'div', {'class': 'plate-model-card__content-date-model'}).text

        info['year'] = year
    except Exception as e:
        print('Error! Check input!')

    return info


if __name__ == '__main__':
    print(extract_info_package('KA8330EK'))
    print(extract_info_package('KA0905CB'))
    print(extract_info_package('KA8330E'))
    print(extract_info_package('KA0905CB'))
    print(extract_info_package('KA8330EK'))
    print(extract_info_package('KA0905CB'))
    print(extract_info_package('KA8330EK'))
    print(extract_info_package('KA0905CB'))
    print(extract_info_package('KA8330EK'))
    print(extract_info_package('KA0905CB'))
    print(extract_info_package('KA8330EK'))
    print(extract_info_package('KA0905CB'))
    print(extract_info_package('KA8330EK'))
    print(extract_info_package('KA0905CB'))
