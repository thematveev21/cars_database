from info_parser import extract_info_package
import requests
import re

if __name__ == '__main__':
    page = 1
    plate_number = 1
    while True:
        print('Page:', page)

        source = requests.get(
            f'https://baza-gai.com.ua/search?year_from=1914&page={page}').text
        for plate in set(re.findall(r'[A-Z]{2}[0-9]{4}[A-Z]{2}', source)):
            print('Plate number:', plate_number, 'Plate:', plate)
            print(extract_info_package(plate))
            plate_number += 1

        page += 1
