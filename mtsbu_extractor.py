from selenium.webdriver import Chrome
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
import chromedriver_autoinstaller
from time import sleep
import re

path = chromedriver_autoinstaller.install(cwd=True)


options = Options()
options.add_argument('--headless')
service = Service(path)


class MTSBUExtractor(Chrome):
    def __init__(self, options: Options = options, service: Service = service, keep_alive: bool = True) -> None:
        super().__init__(options, service, keep_alive)

    def get_vin(self, plates):
        self.get('https://policy-web.mtsbu.ua/')
        button = self.find_element(By.ID, 'carNumber-tab')
        button.click()
        sleep(0.5)

        field = self.find_element(By.ID, 'RegNoModel_PlateNumber')
        field.send_keys(plates)

        btn = self.find_element(By.ID, 'submitBtn')
        btn.click()
        sleep(1)
        vin = re.search(
            r'<div class="value">([A-Z0-9]{10,})<\/div>', self.page_source)

        return vin.group(1) if vin else None

    def __del__(self):
        self.quit()
