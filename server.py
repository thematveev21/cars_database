from flask import Flask, request
from info_parser import extract_info_package
import json

app = Flask(__name__)


@app.route('/search', methods=['GET'])
def search():
    plates = request.args.get('q')
    if plates:
        info = extract_info_package(plates.replace(' ', '').upper())
        if len(info) < 3:
            return 'Not found! Check your query!'
        return json.dumps(info)
    return 'Provide query to search!'


if __name__ == "__main__":
    app.run()
