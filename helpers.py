import re


def clear_spaces(string):
    chars = filter(lambda c: c.isalpha(), re.split(r'\s', string.strip()))
    return " ".join(chars)
